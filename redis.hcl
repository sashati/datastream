# emqx job
# Author: Azam Ahmadloo

job "redis" {
    datacenters = ["dc1"]
    type        = "service"
    priority    = 100

    group "emqx" {
        count = 3

        constraint {
            operator = "distinct_hosts"
            value    = "true"
        }

        restart {
            attempts = 10
            interval = "5m"
            delay    = "25s"
            mode     = "delay"
        }

        task "emqx" {
            driver = "docker"
            config {
                image = "redis"
                port_map {
                    redis-port = 6379
                }
            }
            resources {
                cpu     = 100
                memory  = 2048
                network {
                    mbits = 50
                    port "redisPort" {static = 6379}
                }
            }

            service {
                port = "redisPort"
                name = "Redis-port"
            }
        }
    }
}
