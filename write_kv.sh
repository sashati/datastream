#!/bin/sh
SERVER_ADDRESS=mobin1

CONSUL_HTTP_ADDR=http://$SERVER_ADDRESS:8500

consul kv put "kafka/brokers" '"172.21.1.252:9092,172.21.1.253:9092,172.21.1.254:9092"'
consul kv put "kafka/zookeepers" '"172.21.1.252:2181,172.21.1.253:2181,172.21.1.254:2181"'
consul kv put "kafka/node1/id" '"1"'
consul kv put "kafka/node2/id" '"2"'
consul kv put "kafka/node3/id" '"3"'
consul kv put "kafka/node1/zookeeper/servers" '"0.0.0.0:2888:3888;172.21.1.253:2888:3888;172.21.1.254:2888:3888"'
consul kv put "kafka/node2/zookeeper/servers" '"172.21.1.252:2888:3888;0.0.0.0:2888:3888;172.21.1.254:2888:3888"'
consul kv put "kafka/node3/zookeeper/servers" '"172.21.1.252:2888:3888;172.21.1.253:2888:3888;0.0.0.0:2888:3888"'