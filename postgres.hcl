# A job for test with postgres docker image
# Author: Azam Ahmadloo


job "postgres"{
    datacenters = ["dc1"]
    type        = "service"
    priority    = 70

    group "postgresql" {
        count = 2

        # All groups in this job should be scheduled on different hosts.
        constraint {
            operator = "distinct_hosts"
            value       = "true"
        }
        restart {
            attempts = 10
            interval = "5m"
            delay = "25s"
            mode = "delay"
        }

        task "pgsql" {
            driver = "docker"
            config {
                image = "postgres"
                port_map {
                    pgsql-port = 5432
                }
                volumes = [
                    "local/postgresql.conf:/var/lib/pgsql/11/data/postgresql.conf",
                    "local/pg_hba.conf:/var/lib/pgsql/11/data/pg_hba.conf"
                ]
            }
            template {
                destination = "local/postgresql.conf"
                data = <<EOH
                    listen_addresses = '*'		        # what IP address(es) to listen on;
                    port = 5432				# (change requires restart)
                    max_connections = 100			# (change requires restart)
                    shared_buffers = 128MB			# min 128kB
                    dynamic_shared_memory_type = posix	# the default is the first option
                    max_wal_size = 1GB
                    min_wal_size = 80MB
                    log_destination = 'stderr'		# Valid values are combinations of
                    logging_collector = on			# Enable capturing of stderr and csvlog
                    log_directory = 'log'			# directory where log files are written,
                    log_filename = 'postgresql-%a.log'	# log file name pattern,
                    log_truncate_on_rotation = on		# If on, an existing log file with the
                    log_rotation_age = 1d			# Automatic rotation of logfiles will
                    log_rotation_size = 0			# Automatic rotation of logfiles will
                    log_line_prefix = '%m [%p] '		# special values:
                    log_timezone = 'Asia/Tehran'
                    datestyle = 'iso, mdy'
                    timezone = 'Asia/Tehran'
                    lc_messages = 'en_US.UTF-8'			# locale for system error message
                    lc_monetary = 'en_US.UTF-8'			# locale for monetary formatting
                    lc_numeric = 'en_US.UTF-8'			# locale for number formatting
                    lc_time = 'en_US.UTF-8'				# locale for time formatting
		        EOH
            }
            template {
                destination = "local/pg_hba.conf"
                data = <<EOH
                    local   all             all                                     peer
                    host    all             all             0.0.0.0/0            md5
                    host    all             all             ::1/128                 ident
                    local   replication     all                                     peer
                    host    replication     all             127.0.0.1/32            ident
                    host    replication     all             ::1/128                 ident
		        EOH
            }
            resources {
                cpu = 100
                memory = 8192
                network {
                    mbits = 10
                    port "postgresConn" { static = 5432 }
                }
            }

            service {
                #Specifies the name this service will be advertised as in Consul
                #${BASE}  shorthand for ${JOB}-${GROUP}-${TASK}
                name = "postgres-connector"
                port = "postgresConn"
            }
        }
    }
}