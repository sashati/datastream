# Kafka
# Author: Saeed Shariati

job "kafka" {
    datacenters = [ "dc1"]
    type = "service"
    priority    = 100
    
    # Run tasks in serial or parallel (1 for serial)
    update {
        max_parallel = 1
    }

    ###############################
    ###  Kafka Broker group
    ###############################
    
    group "kafka" {
        count = 3
        constraint {
            attribute = "${meta.zookeeper}"
            value = "true"
        }
        # All groups in this job should be scheduled on different hosts.
        constraint {
            operator  = "distinct_hosts"
            value     = "true"
        }

        restart {
            attempts = 10
            interval = "5m"
            delay = "25s"
            mode = "delay"
        }

        ephemeral_disk {
            migrate = true
            size    = "10240" #MB
            sticky  = true
        }
        task "kafka" {
            driver = "docker"
            config {
                image = "confluentinc/cp-enterprise-kafka:5.3.0"
                labels {
                    group = "kafka"
                }
                port_map {
                    kafka = 9092
                }
                volumes = [
                    "data:/var/lib/kafka/data/",
                    "kafka-logs:/tmp/kafka-logs"
                ]   
            }
            env {
                KAFKA_HEAP_OPTS="-Xmx8G -Xms8G"
                KAFKA_BROKER_RACK="rack1"
                KAFKA_METRIC_REPORTERS="io.confluent.metrics.reporter.ConfluentMetricsReporter"
                CONFLUENT_METRICS_REPORTER_BOOTSTRAP_SERVERS="185.120.223.26:9092,185.120.223.27:9092,185.120.223.28:9092"
                CONFLUENT_METRICS_REPORTER_ZOOKEEPER_CONNECT="185.120.223.26:2181,185.120.223.27:2181,185.120.223.28:2181"
            }
            template {
                destination = "env"
                env = true
                data = <<EOH
                    KAFKA_BROKER_ID={{with node}}{{$nodeName := (.Node.Node) }}{{$keyName := printf "kafka/%s/id" $nodeName }}{{key $keyName}}{{end}}
                    KAFKA_ADVERTISED_LISTENERS=PLAINTEXT://{{with node}}{{.Node.Address}}:9092{{end}} 
                    KAFKA_ZOOKEEPER_CONNECT={{with node}}{{.Node.Address}}:2181{{end}} 
                    EOH
                change_mode = "restart"
            }
            resources {
                cpu = 100
                memory = 8192
                network {
                    mbits = 10
                    port "kafka" { static = 9092 }
                }
            }
            service {
                port = "kafka"
                name = "kafka"
                tags = [
                    "kafka-${NOMAD_ALLOC_INDEX}"
                ]
                check {
                    name = "kafka_docker_check"
                    type = "tcp"
                    interval = "60s"
                    timeout = "5s"
                }
            }
        }
    }
}
