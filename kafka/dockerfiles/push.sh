#!/bin/sh
VERSION=5.2.2
MODULES=(   zookeeper \
            kafka \
            schema-registry \
            ksql-server \
            kafka-connect \
            kafka-rest 
        )

for m in ${MODULES[*]}
do
    echo Push docker image of $m
    docker push registry.aiegroup.ir/core/$m:${VERSION}
done
