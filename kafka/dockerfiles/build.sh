#!/bin/sh
VERSION=5.2.2
MODULES=(   zookeeper \
            kafka \
            schema-registry \
            ksql-server \
            kafka-connect \
            kafka-rest 
        )

for m in ${MODULES[*]}
do
    echo Build docker image of $m
    docker build . -t registry.aiegroup.ir/core/$m:${VERSION} -f $m.Dockerfile
done
