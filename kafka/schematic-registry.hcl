# Kafka
# Author: Saeed Shariati

job "schema-registry" {
    datacenters = [ "dc1"]
    type = "service"
    priority    = 100
    
    # Run tasks in serial or parallel (1 for serial)
    update {
        max_parallel = 1
    }

    ###############################
    ###  Kafka Schema Registry group
    ###############################
    group "schema-registry" {
        count = 3
        # All groups in this job should be scheduled on different hosts.
        constraint {
            operator  = "distinct_hosts"
            value     = "true"
        }

        restart {
            attempts = 10
            interval = "5m"
            delay = "25s"
            mode = "delay"
        }

        ephemeral_disk {
            size    = "128" #MB
            sticky  = false
        }

        task "schema-registry" {
            driver = "docker"
            config {
                image = "confluentinc/cp-schema-registry:5.3.0"
                labels {
                    group = "kafka-schema-registry"
                }
                port_map {
                    schema = 8081
                }
            }
            env {
                KAFKA_HEAP_OPTS="-Xmx2G -Xms2G"

                SCHEMA_REGISTRY_KAFKASTORE_CONNECTION_URL=""
                SCHEMA_REGISTRY_HOST_NAME = "kafka-schema-registry"
                SCHEMA_REGISTRY_LISTENERS = "http://0.0.0.0:8081"
                SCHEMA_REGISTRY_KAFKASTORE_REQUEST_TIMEOUT_MS = 20000
                SCHEMA_REGISTRY_KAFKASTORE_RETRY_BACKOFF_MS = 500
            }
            template {
                destination = "env"
                env = true
                data = <<EOH
                    SCHEMA_REGISTRY_KAFKASTORE_CONNECTION_URL={{with node}}{{.Node.Address}}:2181{{end}} 
                    EOH
                change_mode = "restart"
            }
            resources {
                cpu = 100
                memory = 2048
                network {
                    mbits = 10
                    port "schema" { static = 8081 }
                }
            }
            service {
                port = "schema"
                name = "kafka-schema-registry"
                tags = [
                    "kafka-schema-registry-${NOMAD_ALLOC_INDEX}"
                ]
                check {
                    name = "kafka-schema-registry_docker_check"
                    type = "tcp"
                    interval = "60s"
                    timeout = "5s"
                }
            }
        }
    }
}
