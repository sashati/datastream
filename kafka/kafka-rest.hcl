# Schema Registry
# Author: Saeed Shariati


job "kafka-rest" {
    datacenters = [ "dc1"]
    type = "service"
    priority    = 90
    
    # Run tasks in serial or parallel (1 for serial)
    update {
        max_parallel = 1
    }

     ###############################
    ###  Kafka Rest Proxy group
    ###############################
    group "rest-proxy" {
        count = 3

        restart {
            attempts = 10
            interval = "5m"
            delay = "25s"
            mode = "delay"
        }

        ephemeral_disk {
            size    = "128" #MB
            sticky  = false
        }

        task "kafka-rest" {
            driver = "docker"
            config {
                image = "confluentinc/cp-kafka-rest:5.3.0"
                labels {
                    group = "kafka-rest"
                }
                port_map {
                    proxy = 8082
                }
            }
            env {
                KAFKA_HEAP_OPTS="-Xmx2G -Xms2G"
                KAFKA_REST_LISTENERS="http://0.0.0.0:8082"
                KAFKA_REST_REQUEST_TIMEOUT_MS=20000
                KAFKA_REST_RETRY_BACKOFF_MS=500
                KAFKA_REST_CLIENT_BOOTSTRAP_SERVERS="185.120.223.26:9092,185.120.223.27:9092,185.120.223.28:9092"
                KAFKA_REST_BOOTSTRAP_SERVERS="185.120.223.26:9092,185.120.223.27:9092,185.120.223.28:9092"
                KAFKA_REST_ZOOKEEPER_CONNECT="185.120.223.26:2181,185.120.223.27:2181,185.120.223.28:2181"
            }
            template {
                destination = "env"
                env = true
                data = <<EOH
                KAFKA_REST_SCHEMA_REGISTRY_URL={{with node}}http://{{.Node.Address}}:8081{{end}}
                KAFKA_REST_HOST_NAME={{with node}}{{.Node.Address}}{{end}}
                EOH
                change_mode = "restart"
            }
            resources {
                cpu = 100
                memory = 2048
                network {
                    mbits = 10
                    port "proxy" { static = 8082 }
                }
            }
            service {
                port = "proxy"
                name = "kafka-rest"
                tags = [
                    "kafka-rest-${NOMAD_ALLOC_INDEX}"
                ]
                check {
                    name = "kafka-rest_docker_check"
                    type = "tcp"
                    interval = "60s"
                    timeout = "5s"
                }
            }
        }
    }
}