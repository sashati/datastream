
job "kafka-ksql"{
    datacenters = [ "dc1"]
    type = "service"
    priority    = 96

    # Run tasks in serial or parallel (1 for serial)
    update {
        max_parallel = 1
    }
    ###############################
    ###  Kafka KSQL group
    ###############################
    group "kafka-ksql" {
        count = 3
        # All groups in this job should be scheduled on different hosts.
        constraint {
            operator  = "distinct_hosts"
            value     = "true"
        }

        restart {
            attempts = 10
            interval = "5m"
            delay = "25s"
            mode = "delay"
        }

        ephemeral_disk {
            migrate = false
            size    = "128" #MB
            sticky  = false
        }

        task "kafka-ksql" {
            driver = "docker"
            config {
                image = "confluentinc/cp-ksql-server:5.3.0"
                labels {
                    group = "ksql"
                }
                port_map {
                    ksql = 8088
                }
            }
            env {
                KAFKA_HEAP_OPTS="-Xmx8G -Xms8G"

                KSQL_LISTENERS = "http://0.0.0.0:8088"
                KSQL_BOOTSTRAP_SERVERS="185.120.223.26:9092,185.120.223.27:9092,185.120.223.28:9092"
                SCHEMA_REGISTRY_KAFKASTORE_CONNECTION_URL="185.120.223.26:2181,185.120.223.27:2181,185.120.223.28:2181"
                KSQL_KSQL_SINK_REPLICAS=3
                KSQL_KSQL_STREAMS_REPLICATION_FACTOR=3
            }
             template {
                destination = "env"
                env = true
                data = <<EOH
                    KSQL_KSQL_SCHEMA_REGISTRY_URL = {{with node}}http://{{.Node.Address}}:8081{{end}}
                    EOH
                change_mode = "restart"
            }
            resources {
                cpu = 2000
                memory = 2048
                network {
                    mbits = 10
                    port "ksql" { static = 8088 }
                }
            }
            service {
                port = "ksql"
                name = "ksql"
                tags = [
                    "ksql-${NOMAD_ALLOC_INDEX}"
                ]
                check {
                    name = "ksql_docker_check"
                    type = "tcp"
                    interval = "60s"
                    timeout = "5s"
                }
            }
        }
    }
}
