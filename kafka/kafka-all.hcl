# Kafka
# Author: Saeed Shariati

# Each node in the service should have a metadata variable "${meta.zookeeper}" with value=true to select
# as a client to run in this job.
# To mark each service we user key/value of Consul with below configurations: 
#   kafka/THMDV-HCPHSTA/id = 1
#   kafka/THMDV-HCPHSTB/id = 2
#   kafka/THMDV-HCPHSTC/id = 3
#  for servers
#   kafka/THMDV-HCPHSTA/zookeeper/servers =  \
#         "0.0.0.0:2888:3888;192.168.245.64:2888:3888;192.168.245.65:2888:3888" ...
#
# Service fetch these values from consul and use it to configure the `myid` and `zoo.cnf`


job "kafka" {
    datacenters = [ "dc1"]
    type = "service"
    priority    = 100
    
    # Run tasks in serial or parallel (1 for serial)
    update {
        max_parallel = 1
    }

    ###############################
    ###  Kafka Zookeeper group
    ###############################
    group "zookeeper" {
        count = 3
        constraint {
            attribute = "${meta.zookeeper}"
            value = "true"
        }
        # All groups in this job should be scheduled on different hosts.
        constraint {
            operator  = "distinct_hosts"
            value     = "true"
        }

        restart {
            attempts = 10
            interval = "5m"
            delay = "25s"
            mode = "delay"
        }

        ephemeral_disk {
            migrate = true
            size    = "4096" #MB
            sticky  = true
        }

        task "kafka-zookeeper" {
            driver = "docker"
            config {
                image = "registry.aiegroup.ir/core/zookeeper:5.2.2"
                labels {
                    group = "zk-docker"
                }
                port_map {
                    zk = 2181
                    peer = 2888
                    election = 3888
                }
                volumes = [
                    "data:/var/lib/zookeeper/data",
                    "zookeeper-logs:/var/lib/zookeeper/log"
                ]
            }
            env {
                KAFKA_HEAP_OPTS="-Xmx2G -Xms2G"

                ZOOKEEPER_CLIENT_PORT = 2181
                ZOOKEEPER_TICK_TIME = 2000
                ZOOKEEPER_INIT_LIMIT = 10
                ZOOKEEPER_SYNC_LIMIT = 5
            }
            template {
                destination = "env"
                env = true
                data = <<EOH
                    ZOOKEEPER_SERVER_ID={{with node}}{{$nodeName := (.Node.Node) }}{{$keyName := printf "kafka/%s/id" $nodeName }}{{key $keyName}}{{end}}
                    ZOOKEEPER_SERVERS={{with node}}{{$nodeName := (.Node.Node) }}{{$keyName := printf "kafka/%s/zookeeper/servers" $nodeName }}{{key $keyName}}{{end}}
                    EOH
                change_mode = "restart"
            }
            resources {
                cpu = 100
                memory = 3048
                network {
                    mbits = 1000
                    port "zk" { static = 2181 }
                    port "peer" { static = 2888 }
                    port "election" { static = 3888 }
                }
            }
            service {
                port = "zk"
                name = "kafka-zookeeper"
                tags = [
                    "kafka-zookeeper-${NOMAD_ALLOC_INDEX}"
                ]
                check {
                    name = "kafka-zookeeper_docker_check"
                    type = "tcp"
                    interval = "60s"
                    timeout = "5s"
                }
            }
        }
    }

    ###############################
    ###  Kafka Broker group
    ###############################
    
    group "kafka" {
        count = 3
        constraint {
            attribute = "${meta.zookeeper}"
            value = "true"
        }
        # All groups in this job should be scheduled on different hosts.
        constraint {
            operator  = "distinct_hosts"
            value     = "true"
        }

        restart {
            attempts = 10
            interval = "5m"
            delay = "25s"
            mode = "delay"
        }

        ephemeral_disk {
            migrate = true
            size    = "10240" #MB
            sticky  = true
        }
        task "kafka" {
            driver = "docker"
            config {
                image = "registry.aiegroup.ir/core/kafka:5.2.2"
                labels {
                    group = "kafka"
                }
                port_map {
                    kafka = 9092
                }
                volumes = [
                    "data:/var/lib/kafka/data/",
                    "kafka-logs:/tmp/kafka-logs"
                ]   
            }
            env {
                KAFKA_HEAP_OPTS="-Xmx8G -Xms8G"
            }
            template {
                destination = "env"
                env = true
                data = <<EOH
                    KAFKA_BROKER_ID={{with node}}{{$nodeName := (.Node.Node) }}{{$keyName := printf "kafka/%s/id" $nodeName }}{{key $keyName}}{{end}}
                    KAFKA_ADVERTISED_LISTENERS=PLAINTEXT://{{with node}}{{.Node.Address}}:9092{{end}} 
                    KAFKA_ZOOKEEPER_CONNECT={{with node}}{{.Node.Address}}:2181{{end}} 
                    EOH
                change_mode = "restart"
            }
            resources {
                cpu = 100
                memory = 8192
                network {
                    mbits = 10
                    port "kafka" { static = 9092 }
                }
            }
            service {
                port = "kafka"
                name = "kafka"
                tags = [
                    "kafka-${NOMAD_ALLOC_INDEX}"
                ]
                check {
                    name = "kafka_docker_check"
                    type = "tcp"
                    interval = "60s"
                    timeout = "5s"
                }
            }
        }
    }

    ###############################
    ###  Kafka Schema Registry group
    ###############################
    group "schema-registry" {
        count = 3
        # All groups in this job should be scheduled on different hosts.
        constraint {
            operator  = "distinct_hosts"
            value     = "true"
        }

        restart {
            attempts = 10
            interval = "5m"
            delay = "25s"
            mode = "delay"
        }

        ephemeral_disk {
            size    = "128" #MB
            sticky  = false
        }

        task "schema-registry" {
            driver = "docker"
            config {
                image = "registry.aiegroup.ir/core/schema-registry:5.2.2"
                labels {
                    group = "kafka-schema-registry"
                }
                port_map {
                    schema = 8081
                }
            }
            env {
                KAFKA_HEAP_OPTS="-Xmx2G -Xms2G"

                SCHEMA_REGISTRY_KAFKASTORE_CONNECTION_URL=""
                SCHEMA_REGISTRY_HOST_NAME = "kafka-schema-registry"
                SCHEMA_REGISTRY_LISTENERS = "http://0.0.0.0:8081"
                SCHEMA_REGISTRY_KAFKASTORE_REQUEST_TIMEOUT_MS = 20000
                SCHEMA_REGISTRY_KAFKASTORE_RETRY_BACKOFF_MS = 500
            }
            template {
                destination = "env"
                env = true
                data = <<EOH
                    SCHEMA_REGISTRY_KAFKASTORE_CONNECTION_URL={{with node}}{{.Node.Address}}:2181{{end}} 
                    EOH
                change_mode = "restart"
            }
            resources {
                cpu = 100
                memory = 2048
                network {
                    mbits = 10
                    port "schema" { static = 8081 }
                }
            }
            service {
                port = "schema"
                name = "kafka-schema-registry"
                tags = [
                    "kafka-schema-registry-${NOMAD_ALLOC_INDEX}"
                ]
                check {
                    name = "kafka-schema-registry_docker_check"
                    type = "tcp"
                    interval = "60s"
                    timeout = "5s"
                }
            }
        }
    }

    ###############################
    ###  Kafka KSQL group
    ###############################
    group "ksql" {
        count = 3
        # All groups in this job should be scheduled on different hosts.
        constraint {
            operator  = "distinct_hosts"
            value     = "true"
        }
        
        restart {
            attempts = 10
            interval = "5m"
            delay = "25s"
            mode = "delay"
        }

        ephemeral_disk {
            size    = "128" #MB
            sticky  = false
        }

        task "ksql" {
            driver = "docker"
            config {
                image = "registry.aiegroup.ir/core/ksql-server:5.2.2"
                labels {
                    group = "ksql"
                }
                port_map {
                    ksql = 8088
                }
            }
            env {
                KAFKA_HEAP_OPTS="-Xmx8G -Xms8G"

                KSQL_LISTENERS = "http://0.0.0.0:8088"
            }
             template {
                destination = "env"
                env = true
                data = <<EOH
                    SCHEMA_REGISTRY_KAFKASTORE_CONNECTION_URL={{with node}}{{.Node.Address}}:2181{{end}} 
                    KSQL_BOOTSTRAP_SERVERS = {{with node}}{{.Node.Address}}:9092{{end}} 
                    KSQL_KSQL_SCHEMA_REGISTRY_URL = {{with node}}http://{{.Node.Address}}:8081{{end}}
                    EOH
                change_mode = "restart"
            }
            resources {
                cpu = 100
                memory = 2048
                network {
                    mbits = 10
                    port "ksql" { static = 8088 }
                }
            }
            service {
                port = "ksql"
                name = "ksql"
                tags = [
                    "ksql-${NOMAD_ALLOC_INDEX}"
                ]
                check {
                    name = "ksql_docker_check"
                    type = "tcp"
                    interval = "60s"
                    timeout = "5s"
                }
            }
        }
    }
}
