# Kafka
# Author: Saeed Shariati

job "kafka-control-center" {
    datacenters = [ "dc1"]
    type = "service"
    priority    = 90
    
    # Run tasks in serial or parallel (1 for serial)
    update {
        max_parallel = 1
    }

    ###############################
    ###  Kafka Broker group
    ###############################
    
    group "kafka-control-center" {
        count = 1

        restart {
            attempts = 10
            interval = "5m"
            delay = "25s"
            mode = "delay"
        }

        ephemeral_disk {
            size    = "128" #MB
            sticky  = false
        }
        task "kafka-control-center" {
            driver = "docker"
            config {
                image = "confluentinc/cp-enterprise-control-center:5.3.0"
                labels {
                    group = "kafka-control-center"
                }
                port_map {
                    kcc = 9021
                }
            }
            env {
                KAFKA_HEAP_OPTS="-Xmx2G -Xms2G"
                
                CONTROL_CENTER_REPLICATION_FACTOR=3
                CONTROL_CENTER_MONITORING_INTERCEPTOR_TOPIC_REPLICATION=3
                CONTROL_CENTER_INTERNAL_TOPICS_REPLICATION=3
                CONTROL_CENTER_COMMAND_TOPIC_REPLICATION=3
                CONTROL_CENTER_METRICS_TOPIC_REPLICATION=3
                CONFLUENT_METRICS_TOPIC_REPLICATION=3
                CONTROL_CENTER_STREAMS_NUM_STREAM_THREADS=3
                CONTROL_CENTER_INTERNAL_TOPICS_PARTITIONS=1
                CONTROL_CENTER_MONITORING_INTERCEPTOR_TOPIC_PARTITIONS=1
                # Workaround for MMA-3564
                CONTROL_CENTER_METRICS_TOPIC_MAX_MESSAGE_BYTES=8388608
                PORT=9021
                KAFKA_BROKER_RACK="rack1"
                KAFKA_METRIC_REPORTERS="io.confluent.metrics.reporter.ConfluentMetricsReporter"
            }
            template {
                destination = "env"
                env = true
                data = <<EOH
                    KAFKA_BROKER_ID={{with node}}{{$nodeName := (.Node.Node) }}{{$keyName := printf "kafka/%s/id" $nodeName }}{{key $keyName}}{{end}}
                    KAFKA_ADVERTISED_LISTENERS=PLAINTEXT://{{with node}}{{.Node.Address}}:9092{{end}} 
                    KAFKA_ZOOKEEPER_CONNECT={{with node}}{{.Node.Address}}:2181{{end}} 
                    CONTROL_CENTER_SCHEMA_REGISTRY_URL=http://{{with node}}{{.Node.Address}}:8081{{end}} 
                    CONTROL_CENTER_CONNECT_CLUSTER={{with node}}{{.Node.Address}}:8083{{end}} 
                    CONTROL_CENTER_KSQL_ADVERTISED_URL=http://{{with node}}{{.Node.Address}}:8088{{end}} 
                    CONTROL_CENTER_KSQL_URL=http://{{with node}}{{.Node.Address}}:8088{{end}} 
                    CONFLUENT_METRICS_REPORTER_ZOOKEEPER_CONNECT={{key "kafka/zookeepers"}}
                    CONFLUENT_METRICS_REPORTER_BOOTSTRAP_SERVERS={{key "kafka/brokers"}}
                    CONTROL_CENTER_BOOTSTRAP_SERVERS={{key "kafka/brokers"}}
                    EOH
                change_mode = "restart"
            }
            resources {
                cpu = 100
                memory = 2048
                network {
                    mbits = 10
                    port "kcc" { static = 9021 }
                }
            }
            service {
                port = "kcc"
                name = "kcc"
                tags = [
                    "kcc-${NOMAD_ALLOC_INDEX}"
                ]
                check {
                    name = "kafka_control_center_check"
                    type = "tcp"
                    interval = "60s"
                    timeout = "5s"
                }
            }
        }
    }
}
