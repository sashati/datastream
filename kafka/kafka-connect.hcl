# Kafka
# Author: Saeed Shariati

job "kafka-connect" {
    datacenters = [ "dc1"]
    type = "service"
    priority    = 97

    # Run tasks in serial or parallel (1 for serial)
    update {
        max_parallel = 1
    }
    ###############################
    ###  Kafka Connect group
    ###############################
    group "kafka-connect" {
        count = 3
        # All groups in this job should be scheduled on different hosts.
        constraint {
            operator  = "distinct_hosts"
            value     = "true"
        }

        restart {
            attempts = 10
            interval = "5m"
            delay = "25s"
            mode = "delay"
        }

        ephemeral_disk {
            migrate = false
            #size    = "2048" #MB
            sticky  = false
        }

        task "kafka-connect" {
            driver = "docker"
            config {
                image = "confluentinc/cp-kafka-connect:5.3.0"
                labels {
                    group = "kafka-connect"
                }
                port_map {
                    port = 8083
                }
            }
            env {
                KAFKA_HEAP_OPTS="-Xmx4G -Xms4G"

                CONNECT_REST_PORT=8083
                CONNECT_GROUP_ID="kafka-connect"
                CONNECT_CONFIG_STORAGE_TOPIC="kafka-connect-config"
                CONNECT_OFFSET_STORAGE_TOPIC="kafka-connect-offsets"
                CONNECT_STATUS_STORAGE_TOPIC="kafka-connect-status"
                CONNECT_KEY_CONVERTER="io.confluent.connect.avro.AvroConverter"
                CONNECT_VALUE_CONVERTER="io.confluent.connect.avro.AvroConverter"
                CONNECT_INTERNAL_KEY_CONVERTER="org.apache.kafka.connect.json.JsonConverter"
                CONNECT_INTERNAL_VALUE_CONVERTER="org.apache.kafka.connect.json.JsonConverter"
                CONNECT_PLUGIN_PATH="/usr/share/java,/etc/kafka-connect/jars"
                CONNECT_BOOTSTRAP_SERVERS="185.120.223.26:9092,185.120.223.27:9092,185.120.223.28:9092"
                KAFKA_ZOOKEEPER_CONNECT="185.120.223.26:2181,185.120.223.27:2181,185.120.223.28:2181"
            }
            template {
                destination = "env"
                env = true
                data = <<EOH
                    CONNECT_KEY_CONVERTER_SCHEMA_REGISTRY_URL = {{with node}}{{.Node.Address}}:8081{{end}}
                    CONNECT_VALUE_CONVERTER_SCHEMA_REGISTRY_URL = {{with node}}http://{{.Node.Address}}:8081{{end}}
                    CONNECT_REST_ADVERTISED_HOST_NAME = {{with node}}{{.Node.Address}}{{end}}
                    EOH
                change_mode = "restart"
            }
            resources {
                cpu = 1000
                memory = 4200
                network {
                    mbits = 10
                    port "port" { static = 8083 }
                }
            }
            service {
                port = "port"
                name = "kafka-connect"
                tags = [
                    "connect-${NOMAD_ALLOC_INDEX}"
                ]
                check {
                    name = "connect_docker_check"
                    type = "tcp"
                    interval = "60s"
                    timeout = "5s"
                }
            }
        }
    }
}
