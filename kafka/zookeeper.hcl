# Kafka
# Author: Saeed Shariati

# Each node in the service should have a metadata variable "${meta.zookeeper}" with value=true to select
# as a client to run in this job.
# To mark each service we user key/value of Consul with below configurations: 
#   kafka/THMDV-HCPHSTA/id = 1
#   kafka/THMDV-HCPHSTB/id = 2
#   kafka/THMDV-HCPHSTC/id = 3
#  for servers
#   kafka/THMDV-HCPHSTA/zookeeper/servers =  \
#         "0.0.0.0:2888:3888;192.168.245.64:2888:3888;192.168.245.65:2888:3888" ...
#
# Service fetch these values from consul and use it to configure the `myid` and `zoo.cnf`


job "zookeeper" {
    datacenters = [ "dc1"]
    type = "service"
    priority    = 100
    
    # Run tasks in serial or parallel (1 for serial)
    update {
        max_parallel = 1
    }

    ###############################
    ###  Kafka Zookeeper group
    ###############################
    group "zookeeper" {
        count = 3
        constraint {
            attribute = "${meta.zookeeper}"
            value = "true"
        }
        # All groups in this job should be scheduled on different hosts.
        constraint {
            operator  = "distinct_hosts"
            value     = "true"
        }

        restart {
            attempts = 10
            interval = "5m"
            delay = "25s"
            mode = "delay"
        }

        ephemeral_disk {
            migrate = true
            size    = "4096" #MB
            sticky  = true
        }

        task "kafka-zookeeper" {
            driver = "docker"
            config {
                image = "confluentinc/cp-zookeeper:5.3.0"
                labels {
                    group = "zk-docker"
                }
                port_map {
                    zk = 2181
                    peer = 2888
                    election = 3888
                }
                volumes = [
                    "data:/var/lib/zookeeper/data",
                    "zookeeper-logs:/var/lib/zookeeper/log"
                ]
            }
            env {
                KAFKA_HEAP_OPTS="-Xmx2G -Xms2G"

                ZOOKEEPER_CLIENT_PORT = 2181
                ZOOKEEPER_TICK_TIME = 2000
                ZOOKEEPER_INIT_LIMIT = 10
                ZOOKEEPER_SYNC_LIMIT = 5
            }
            template {
                destination = "env"
                env = true
                data = <<EOH
                    ZOOKEEPER_SERVER_ID={{with node}}{{$nodeName := (.Node.Node) }}{{$keyName := printf "kafka/%s/id" $nodeName }}{{key $keyName}}{{end}}
                    ZOOKEEPER_SERVERS={{with node}}{{$nodeName := (.Node.Node) }}{{$keyName := printf "kafka/%s/zookeeper/servers" $nodeName }}{{key $keyName}}{{end}}
                    EOH
                change_mode = "restart"
            }
            resources {
                cpu = 100
                memory = 3048
                network {
                    mbits = 1000
                    port "zk" { static = 2181 }
                    port "peer" { static = 2888 }
                    port "election" { static = 3888 }
                }
            }
            service {
                port = "zk"
                name = "kafka-zookeeper"
                tags = [
                    "kafka-zookeeper-${NOMAD_ALLOC_INDEX}"
                ]
                check {
                    name = "kafka-zookeeper_docker_check"
                    type = "tcp"
                    interval = "60s"
                    timeout = "5s"
                }
            }
        }
    }
}
